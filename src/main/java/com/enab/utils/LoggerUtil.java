package com.enab.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * 日志工具
 */
@Component
public class LoggerUtil {


    /**
     * 日志工具bean
     *
     * @param clazz
     * @return
     */
    public static Logger loggerFactory(Class<?> clazz) {
        return LoggerFactory.getLogger(clazz);
    }
}
