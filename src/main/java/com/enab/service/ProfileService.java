package com.enab.service;

import com.enab.dao.*;
import com.enab.model.pojo.*;
import com.enab.utils.DateUtil;
import com.enab.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class ProfileService {
    @Autowired
    private ProfileDao profileDao;

    @Autowired
    private BlogDao blogDao;

    public List<Profile> selectProfileList(){
        return profileDao.selectProfileList();
    }

    public Profile findUserProfileById(Integer id){
        Profile profile = profileDao.findProfileById(id);
        List<Blog> blogList = blogDao.selectBlogListByProfileId(profile.getId());
        profile.setBlogList(blogList);
        return profile;
    }
}
