package com.enab.config;

public class RabbitMQConfig {
    /**
     * 邮件消息队列 名
     */
    public static final String MAIL_QUEUE = "MAIL";

    /**
     * 博客更改 消息队列 名
     */
    public static final String BLOG_QUEUE = "BLOG";
}
