package com.enab.model.pojo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户
 */
@Data
@ToString
@ApiModel("enab好友")
public class Profile implements Serializable {

    /**
     * user(36) => 1436499(10)
     */
    private static final long serialVersionUID = 1436490L;


    @ApiModelProperty(value = "profile id", dataType = "Integer")
    private Integer id;

    @ApiModelProperty(value = "user id", dataType = "Integer")
    private Integer userId;

    @ApiModelProperty(value = "名字", dataType = "String")
    private String name;

    @ApiModelProperty(value = "性别", dataType = "String")
    private String sex;

    @ApiModelProperty(value = "生日", dataType = "String")
    private Date birthday;

    @ApiModelProperty(value = "微信号", dataType = "String")
    private String wechat;

    @ApiModelProperty(value = "mailbox", dataType = "String")
    private String mailbox;

    @ApiModelProperty(value = "company", dataType = "String")
    private String company;

    @ApiModelProperty(value = "position", dataType = "String")
    private String position;

    @ApiModelProperty(value = "recommender", dataType = "String")
    private String recommender;

    @ApiModelProperty(value = "isLecture", dataType = "String")
    private String isLecture;

    @ApiModelProperty(value = "isVolunteer", dataType = "String")
    private String isVolunteer;

    @ApiModelProperty(value = "介绍")
    private String referral;

    @ApiModelProperty(value = "封面图")
    private String img;

    @ApiModelProperty(value = "擅长领域")
    private String adept;

    @ApiModelProperty(value = "文章列表")
    private List<Blog> blogList;
}