package com.enab.model.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 专家关联文章
 */
@Data
@ToString
@ApiModel("专家关联文章表")
public class Expert implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "profile id", dataType = "Integer")
    private Integer profileId;

    @ApiModelProperty(value = "blog id", dataType = "Integer")
    private Integer blogId;


    private Profile profile;

    private List<Blog> blogList;

}
