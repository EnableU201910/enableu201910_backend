package com.enab.controller;

import com.enab.model.entity.Result;
import com.enab.model.entity.StatusCode;
import com.enab.service.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "专家api", description = "专家api", basePath = "/expert")
@RestController
@RequestMapping("/expert")
public class ExpertController {

    @Autowired
    private ProfileService profileService;


    /**
     * 专家列表查询
     *
     * @return
     */
    @ApiOperation(value = "专家列表")
    @PostMapping("/expert/list")
    @ResponseBody
    public Result expertProfile() {
        return Result.create(StatusCode.OK, "查询成功", profileService.selectProfileList());
    }

    /**
     * 专家详情
     *
     * @param profileId      专家ID
     * @return
     */
    @ApiOperation(value = "专家详情")
    @PostMapping("/expert/{profileId}")
    @ResponseBody
    public Result expert(@PathVariable Integer profileId) {

        return Result.create(StatusCode.OK, "查询成功", profileService.findUserProfileById(profileId));
    }

}
