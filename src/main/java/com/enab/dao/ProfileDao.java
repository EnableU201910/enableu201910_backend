package com.enab.dao;

import com.enab.model.pojo.Profile;
import com.enab.model.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ProfileDao {

    /**
     * 根据用户id查询用户profile
     *
     * @param id
     * @return
     */
    Profile findUserProfileById(Integer id);



    /**
     * 保存用户
     *
     * @param profile
     */
    void saveUserProfile(Profile profile);


    /**
     * 根据用户id更新profile
     * 需要id
     *
     * @param profile
     */
    void updateUserProfileById(Profile profile);

    List<Profile> selectProfileList();

    /**
     * 根据用户id查询用户profile
     *
     * @param id
     * @return
     */
    Profile findProfileById(Integer id);
}
