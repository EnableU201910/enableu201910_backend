package com.enab.dao;

import com.enab.model.pojo.Expert;
import com.enab.model.pojo.Profile;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ExpertDao {

    /**
     * 关联专家和文章
     *
     * @param expert
     */
    void saveProfileBlog(Expert expert);


}
